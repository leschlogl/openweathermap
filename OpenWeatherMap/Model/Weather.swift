//
//  Weather.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import ObjectMapper
import MapKit

class WeatherResponse: Mappable {
    var weathers: [Weather]?

    required init?(map: Map) {}

    func mapping(map: Map) {
        weathers <- map["list"]
    }
}

class Weather: Mappable {
    var coordinates: CLLocationCoordinate2D?
    var condition: WeatherCondition?
    var info: WeatherInfos?
    var sunrise: Date?
    var sunset: Date?
    var date: Date?
    var cityName: String?
    var forecast: Forecast?
    var id: Int?

    required init?(map: Map) {}

    func mapping(map: Map) {
        var lat: Double?
        var lon: Double?
        lat <- map["coord.lat"]
        lon <- map["coord.lon"]

        if let lat = lat,
            let lon = lon {
            coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }

        var conditionArray: [WeatherCondition]?
        conditionArray <- map["weather"]
        if let conditionArray = conditionArray,
            conditionArray.count > 0 {
            condition = conditionArray[0]
        }

        var sunriseTimestamp: Int?
        var sunsetTimestamp: Int?
        var dateTimeStamp: Int?

        sunriseTimestamp <- map["sys.sunrise"]
        sunsetTimestamp <- map["sys.sunset"]
        dateTimeStamp <- map["dt"]

        sunrise = ConversionUtils.getDateFromUnixTimestamp(timestamp: sunriseTimestamp)
        sunset = ConversionUtils.getDateFromUnixTimestamp(timestamp: sunsetTimestamp)
        date = ConversionUtils.getDateFromUnixTimestamp(timestamp: dateTimeStamp)

        info <- map["main"]
        cityName <- map["name"]
        id <- map["id"]

    }

}

class WeatherCondition: Mappable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?

    required init?(map: Map) {}

    func mapping(map: Map) {
        id <- map["id"]
        main <- map["main"]
        description <- map["description"]
        icon <- map["icon"]
    }
}

class WeatherInfos: Mappable {
    var temp: Float?
    var pressure: Int?
    var humidity: Int?
    var temp_min: Int?
    var temp_max: Int?

    required init?(map: Map) {}

    func mapping(map: Map) {
        temp <- map["temp"]
        pressure <- map["pressure"]
        humidity <- map["humidity"]
        temp_min <- map["temp_min"]
        temp_max <- map["temp_max"]
    }
}

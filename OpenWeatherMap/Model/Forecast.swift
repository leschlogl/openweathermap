//
//  Forecast.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import ObjectMapper

class ForecastResponse: Mappable {
    var forecasts: [Forecast]?

    required init?(map: Map) {}

    func mapping(map: Map) {
        forecasts <- map["list"]
    }
}

class Forecast: Mappable {
    var date: Date?
    var temperature: ForecastTemperature?
    var pressure: Float?
    var weatherCondition: WeatherCondition?

    required init?(map: Map) {}

    func mapping(map: Map) {
        var conditionArray: [WeatherCondition]?
        conditionArray <- map["weather"]
        if let conditionArray = conditionArray,
            conditionArray.count > 0 {
            weatherCondition = conditionArray[0]
        }
        temperature <- map["temp"]
        var dateTimeStamp: Int?
        dateTimeStamp <- map["dt"]
        date = ConversionUtils.getDateFromUnixTimestamp(timestamp: dateTimeStamp)
    }
}
class ForecastTemperature: Mappable {
    var day: Float?
    var night: Float?
    var morning: Float?
    var evening: Float?
    var min: Float?
    var max: Float?

    required init?(map: Map) {}

    func mapping(map: Map) {
        day <- map["day"]
        night <- map["night"]
        morning <- map["morn"]
        evening <- map["eve"]
        max <- map["max"]
        min <- map["min"]
    }
}

//
//  LocationManager.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import CoreLocation

protocol LocationManagerDelegate {
    func locationUpdated(location: CLLocationCoordinate2D)
    func changedAuthorization(status: CLAuthorizationStatus)
}

class LocationManager: CLLocationManager, CLLocationManagerDelegate {

    public var authorizationStatus: CLAuthorizationStatus
    var locationDelegate: LocationManagerDelegate?

    override init() {
        authorizationStatus = CLLocationManager.authorizationStatus()
        super.init()
        desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.delegate = self
    }

    var locationPermissionsGranted: Bool {
        return authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        authorizationStatus = status
        self.locationDelegate?.changedAuthorization(status: status)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = manager.location?.coordinate {
        self.locationDelegate?.locationUpdated(location: coordinate)
        }
    }
}

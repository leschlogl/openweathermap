//
//  ForecastCollectionViewCell.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import UIKit

struct ForecastIdentifier {
    static let reuseId = "ForecastCollectionViewCell.id"
}

class ForecastCollectionViewCell: UICollectionViewCell {

    private lazy var dayLabel = UILabel.newAutoLayout()
    private lazy var iconImage = UILabel.newAutoLayout()
    private lazy var tempLabel = UILabel.newAutoLayout()

    public var forecast: Forecast? {
        didSet {
            if let date = forecast?.date {
                dayLabel.text = ConversionUtils.dayOfTheWeekFromString(date: date)
            }
            if let icon = forecast?.weatherCondition?.icon {
                iconImage.text = ConversionUtils.getIconFromWeatherCondition(condition: icon)
            }
            if let temp = forecast?.temperature?.day {
                tempLabel.text = ConversionUtils.normalizeTemperature(temperature: temp)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    private func setupView() {

        dayLabel.adjustsFontSizeToFitWidth = true
        dayLabel.textAlignment = .center
        contentView.addSubview(dayLabel)
        dayLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 0)
        dayLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 0)
        dayLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 0)
        dayLabel.autoSetDimension(.height, toSize: 20)

        iconImage.font = UIFont.systemFont(ofSize: 30)
        iconImage.adjustsFontSizeToFitWidth = true
        iconImage.textAlignment = .center
        contentView.addSubview(iconImage)
        iconImage.autoPinEdge(.top, to: .bottom, of: dayLabel, withOffset: 5)
        iconImage.autoPinEdge(toSuperviewEdge: .left, withInset: 0)
        iconImage.autoPinEdge(toSuperviewEdge: .right, withInset: 0)
        iconImage.autoSetDimension(.height, toSize: 30)

        tempLabel.adjustsFontSizeToFitWidth = true
        tempLabel.textAlignment = .center
        contentView.addSubview(tempLabel)
        tempLabel.autoPinEdge(.top, to: .bottom, of: iconImage, withOffset: 5)
        tempLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 0)
        tempLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 0)
        tempLabel.autoSetDimension(.height, toSize: 20)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

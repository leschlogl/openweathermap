//
//  WeatherTableViewCell.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import UIKit

struct WeatherIdentifier {
    static let reuseId = "WeatherTableViewCell.id"
}

//Protocol used to open the details screen when touched in some collectionview cell
//hitEvent doesn't work with disabling touchUpInside and enabling scrolling
protocol WeatherTableViewCellDelegate {
    func touchOpenDetails(weather: Weather?)
}

class WeatherTableViewCell: UITableViewCell {

    private lazy var containerView = UIView.newAutoLayout()
    private lazy var weatherIcon = UILabel.newAutoLayout()
    private lazy var cityNameLabel = UILabel.newAutoLayout()
    private lazy var tempLabel = UILabel.newAutoLayout()
    private lazy var loadingIndicator = UIActivityIndicatorView.newAutoLayout()
    private lazy var flowLayout = UICollectionViewFlowLayout()
    private lazy var forecastCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
    private lazy var forecasts: [Forecast] = []

    public var cellDelegate: WeatherTableViewCellDelegate?

    public var weather: Weather? {
        didSet {
            if let icon = weather?.condition?.icon {
                self.weatherIcon.text = ConversionUtils.getIconFromWeatherCondition(condition: icon)
            } else {
                self.weatherIcon.text = "❓"
            }
            self.cityNameLabel.text = weather?.cityName ?? ""
            if let temp = weather?.info?.temp {
                self.tempLabel.text = ConversionUtils.normalizeTemperature(temperature: temp)
            } else {
                self.tempLabel.text = ""
            }
            loadForecasts()
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .clear

        setupLayout()
    }

    private func setupLayout() {

        weatherIcon.font = UIFont.systemFont(ofSize: 60)
        contentView.addSubview(weatherIcon)
        weatherIcon.autoAlignAxis(toSuperviewAxis: .horizontal)
        weatherIcon.autoPinEdge(toSuperviewEdge: .left, withInset: 10)
        weatherIcon.autoSetDimension(.width, toSize: 75)

        containerView.layer.cornerRadius = 5.0
        containerView.backgroundColor = UIColor(white: 0, alpha: 0.1)
        contentView.addSubview(containerView)
        containerView.autoPinEdge(toSuperviewEdge: .top, withInset: 10)
        containerView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 10)
        containerView.autoPinEdge(toSuperviewEdge: .right, withInset: 10)
        containerView.autoPinEdge(.left, to: .right, of: weatherIcon, withOffset: 5)

        tempLabel.font = UIFont.boldSystemFont(ofSize: 30)
        tempLabel.textAlignment = .right
        containerView.addSubview(tempLabel)
        tempLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 10)
        tempLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 20)
        tempLabel.autoSetDimension(.height, toSize: 50)

        cityNameLabel.font = UIFont.systemFont(ofSize: 22)
        cityNameLabel.numberOfLines = 2
        cityNameLabel.adjustsFontSizeToFitWidth = true
        containerView.addSubview(cityNameLabel)
        cityNameLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 10)
        cityNameLabel.autoPinEdge(.right, to: .left, of: tempLabel, withOffset: -10)
        cityNameLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 20)
        cityNameLabel.autoSetDimension(.height, toSize: 50)

        flowLayout.minimumInteritemSpacing = 10
        flowLayout.itemSize = CGSize(width: 60, height: 80)
        flowLayout.scrollDirection = .horizontal

        forecastCollectionView.register(ForecastCollectionViewCell.self, forCellWithReuseIdentifier: ForecastIdentifier.reuseId)
        forecastCollectionView.delegate = self
        forecastCollectionView.dataSource = self
        forecastCollectionView.translatesAutoresizingMaskIntoConstraints = false
        forecastCollectionView.backgroundColor = .clear
        containerView.addSubview(forecastCollectionView)
        forecastCollectionView.autoPinEdge(toSuperviewEdge: .left, withInset: 10)
        forecastCollectionView.autoPinEdge(toSuperviewEdge: .right, withInset: 10)
        forecastCollectionView.autoPinEdge(.top, to: .bottom, of: cityNameLabel, withOffset: 10)
        forecastCollectionView.autoSetDimension(.height, toSize: 80)

        loadingIndicator.style = .whiteLarge
        forecastCollectionView.addSubview(loadingIndicator)
        loadingIndicator.autoAlignAxis(toSuperviewAxis: .horizontal)
        loadingIndicator.autoAlignAxis(toSuperviewAxis: .vertical)

    }

    private func loadForecasts() {
        if let cityId = weather?.id {
            loadingIndicator.startAnimating()
            ForecastRequest.sharedInstance.getForecast(cityId: cityId) { (response, error) in
                self.loadingIndicator.stopAnimating()
                if let response = response,
                    let forecasts = response.forecasts {
                    self.forecasts = forecasts
                    self.forecastCollectionView.reloadData()
                }
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {}

    
}
//MARK: - CollectionView Methods
extension WeatherTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecasts.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ForecastIdentifier.reuseId, for: indexPath) as? ForecastCollectionViewCell
        cell?.forecast = self.forecasts[indexPath.row]
        return cell ?? UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        cellDelegate?.touchOpenDetails(weather: weather)
    }
}

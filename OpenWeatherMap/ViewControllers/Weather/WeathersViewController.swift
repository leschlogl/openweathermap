//
//  WeathersViewController.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import UIKit
import PureLayout

class WeathersViewController: UIViewController {

    private lazy var tableView: UITableView = UITableView.newAutoLayout()
    private lazy var backgroundView: UIImageView = UIImageView.newAutoLayout()
    private lazy var weathers: [Weather] = []
    private lazy var isLoading = false

    private lazy var refreshControl = UIRefreshControl()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Temperaturas"

        let rightButtom = UIBarButtonItem(image: UIImage(named: "MapIcon")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(touchOpenMapView))
        self.navigationItem.rightBarButtonItem = rightButtom

        backgroundView.image = UIImage(named: "WeatherBackground")
        backgroundView.contentMode = .scaleAspectFill
        view.addSubview(backgroundView)
        backgroundView.autoPinEdgesToSuperviewEdges()

        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }

        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(WeatherTableViewCell.self, forCellReuseIdentifier: WeatherIdentifier.reuseId)
        view.addSubview(self.tableView)
        tableView.autoPinEdgesToSuperviewEdges()
    }

    @objc private func loadData() {
        if isLoading {
            return
        }
        isLoading = true
        self.refreshControl.beginRefreshing()

        WeatherRequest.sharedInstance.getWeathers { (response, error) in
            self.isLoading = false
            self.refreshControl.endRefreshing()
            if let response = response,
                let weathers = response.weathers {
                self.weathers = weathers
                self.tableView.reloadData()
            }
        }
    }
    @objc private func touchOpenMapView() {
        self.navigationController?.pushViewController(MapViewController(weathers: self.weathers), animated: true)
    }
    
    private func openDetailsViewController(weather: Weather) {
        self.navigationController?.pushViewController(WeatherDetailsViewController(weather: weather), animated: true)
    }
}

extension WeathersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherIdentifier.reuseId, for: indexPath) as? WeatherTableViewCell
        cell?.weather = self.weathers[indexPath.row]
        cell?.cellDelegate = self
        return cell ?? UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openDetailsViewController(weather: self.weathers[indexPath.row])
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

extension WeathersViewController: WeatherTableViewCellDelegate {
    func touchOpenDetails(weather: Weather?) {
        if let weather = weather {
            openDetailsViewController(weather: weather)
        }
    }
}

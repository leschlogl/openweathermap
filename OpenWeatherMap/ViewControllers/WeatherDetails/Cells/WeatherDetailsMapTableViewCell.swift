//
//  WeatherDetailsMapTableViewCell.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import UIKit
import MapKit

struct WeatherDetailsMapIdentifier {
    static let reuseId = "WeatherDetailsMapTableViewCell.id"
}

class WeatherDetailsMapTableViewCell: UITableViewCell {

    private lazy var mapView = MKMapView.newAutoLayout()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .clear

        setupLayout()
    }

    func setContent(location: CLLocationCoordinate2D) {
        let center = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
        mapView.setRegion(region, animated: true)
    }

    private func setupLayout() {
        mapView.showsUserLocation = false
        mapView.isUserInteractionEnabled = false
        contentView.addSubview(mapView)
        mapView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }

    override func setSelected(_ selected: Bool, animated: Bool) { }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

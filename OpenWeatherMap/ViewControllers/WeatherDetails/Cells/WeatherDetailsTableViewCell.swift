//
//  WeatherDetailsTableViewCell.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import UIKit

struct WeatherDetailsIdentifier {
    static let reuseId = "WeatherDetailsTableViewCell.id"
}

class WeatherDetailsTableViewCell: UITableViewCell {

    private lazy var titleLabel = UILabel.newAutoLayout()
    private lazy var descriptionLabel = UILabel.newAutoLayout()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .clear

        setupLayout()
    }

    func setContent(title: String, description: String) {
        self.titleLabel.text = title
        self.descriptionLabel.text = description
    }

    private func setupLayout() {
        contentView.addSubview(titleLabel)
        titleLabel.autoAlignAxis(toSuperviewAxis: .horizontal)
        titleLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
        titleLabel.autoSetDimension(.width, toSize: 140)

        descriptionLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.textAlignment = .right
        contentView.addSubview(descriptionLabel)
        descriptionLabel.autoAlignAxis(toSuperviewAxis: .horizontal)
        descriptionLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
        descriptionLabel.autoPinEdge(.left, to: .right, of: titleLabel, withOffset: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) { }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

//
//  WeatherDetailsViewController.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import UIKit

struct TableViewMapping {
    var title: String
    var description: String
}

class WeatherDetailsViewController: UIViewController {

    private let weather: Weather!

    private var tableViewMapping:[TableViewMapping] = []

    private lazy var tableView = UITableView.newAutoLayout()

    init(weather: Weather) {
        self.weather = weather
        super.init(nibName: nil, bundle: nil)

        if let cityName = weather.cityName {
            self.title = cityName
        } else {
            self.title = "Detalhes"
        }

        mapTableView()
    }

    private func mapTableView() {
        if let description = weather.condition?.description {
            tableViewMapping.append(TableViewMapping(title: "🌈 Clima", description: description))
        }

        if let time = weather.date {
            tableViewMapping.append(TableViewMapping(title: "⏱ Atualização", description: ConversionUtils.dateFromString(date: time)))
        }

        if let minTemp = weather.info?.temp_min {
            tableViewMapping.append(TableViewMapping(title: "🌡 Temp. mínima", description: ConversionUtils.normalizeTemperature(temperature: Float(minTemp))))
        }

        if let pressure = weather.info?.temp_max {
            tableViewMapping.append(TableViewMapping(title: "🌎 Pressão", description: ConversionUtils.normalizePressure(pressure: pressure)))
        }

        if let humidity = weather.info?.humidity {
            tableViewMapping.append(TableViewMapping(title: "💧 Umidade", description: ConversionUtils.normalizeHumidity(humidity: humidity)))
        }

        if let maxTemp = weather.info?.temp_max {
            tableViewMapping.append(TableViewMapping(title: "🌡 Temp. máxima", description: ConversionUtils.normalizeTemperature(temperature: Float(maxTemp))))
        }

        if let sunrise = weather.sunrise {
            tableViewMapping.append(TableViewMapping(title: "🌅 Nascer do sol", description: ConversionUtils.timeFromString(date: sunrise)))
        }

        if let sunset = weather.sunset {
            tableViewMapping.append(TableViewMapping(title: "🌇 Pôr do sol", description: ConversionUtils.timeFromString(date: sunset)))
        }

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(WeatherDetailsTableViewCell.self, forCellReuseIdentifier: WeatherDetailsIdentifier.reuseId)
        tableView.register(WeatherDetailsMapTableViewCell.self, forCellReuseIdentifier: WeatherDetailsMapIdentifier.reuseId)
        view.addSubview(self.tableView)
        tableView.autoPinEdgesToSuperviewEdges()

    }
}

extension WeatherDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let containerView = UIView()
        containerView.backgroundColor = .white

        let iconView = UILabel.newAutoLayout()
        iconView.font = UIFont.systemFont(ofSize: 50)
        if let icon = weather.condition?.icon {
            iconView.text = ConversionUtils.getIconFromWeatherCondition(condition: icon)
        }
        containerView.addSubview(iconView)
        iconView.autoSetDimension(.width, toSize: 80)
        iconView.autoAlignAxis(toSuperviewAxis: .horizontal)
        iconView.autoPinEdge(toSuperviewEdge: .left, withInset: 15)

        let tempLabel = UILabel.newAutoLayout()
        tempLabel.font = UIFont.boldSystemFont(ofSize: 50)
        if let temp = weather.info?.temp {
            tempLabel.text = ConversionUtils.normalizeTemperature(temperature: temp)
        }
        tempLabel.textAlignment = .right
        containerView.addSubview(tempLabel)
        tempLabel.autoAlignAxis(toSuperviewAxis: .horizontal)
        tempLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
        tempLabel.autoPinEdge(.left, to: .right, of: iconView, withOffset: 10)
        return containerView
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var counter = 0
        if weather.coordinates != nil {
            counter += 1
        }
        return tableViewMapping.count + counter
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < tableViewMapping.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: WeatherDetailsIdentifier.reuseId, for: indexPath) as? WeatherDetailsTableViewCell
            let dict:TableViewMapping = tableViewMapping[indexPath.row]
            cell?.setContent(title: dict.title, description: dict.description)
            return cell ?? UITableViewCell()
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: WeatherDetailsMapIdentifier.reuseId, for: indexPath) as? WeatherDetailsMapTableViewCell
            if let coordinates = weather.coordinates {
                cell?.setContent(location: coordinates)
            }
            return cell ?? UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < tableViewMapping.count {
            return 50
        } else {
            return 150
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
}

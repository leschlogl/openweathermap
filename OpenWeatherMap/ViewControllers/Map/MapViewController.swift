//
//  MapViewController.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    private lazy var mapView = MKMapView.newAutoLayout()
    private lazy var locationManager = LocationManager()
    private lazy var weathers: [Weather] = []
    private lazy var adjustMapZoom = false

    init(weathers: [Weather]) {
        super.init(nibName: nil, bundle: nil)
        self.weathers = weathers
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Mapa"

        mapView.showsUserLocation = true
        view.addSubview(mapView)
        mapView.autoPinEdgesToSuperviewEdges()

        locationManager.locationDelegate = self
        if !locationManager.locationPermissionsGranted {
            locationManager.requestWhenInUseAuthorization()
        }

        addWeatherPins()
    }

    func addWeatherPins() {
        for weather in self.weathers {
            if let location = weather.coordinates,
                let cityName = weather.cityName,
                let temp = weather.info?.temp {
                let pin = MKPointAnnotation()
                pin.title = cityName
                pin.subtitle = ConversionUtils.normalizeTemperature(temperature: temp)
                pin.coordinate = location
                mapView.addAnnotation(pin)
            }
        }
    }
}

extension MapViewController: LocationManagerDelegate {
    func locationUpdated(location: CLLocationCoordinate2D) {
        if adjustMapZoom {
            let center = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
            mapView.setRegion(region, animated: true)
            adjustMapZoom = false
        }
    }

    func changedAuthorization(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
            adjustMapZoom = true
        } else if status == .denied {
            let alertController = UIAlertController.init(title: "Localização desabilitada", message: "Para uma melhor experiência, ative a localização do App", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Abrir configurações", style: .default) { value in
                let path = UIApplication.openSettingsURLString
                if let settingsURL = URL(string: path), UIApplication.shared.canOpenURL(settingsURL) {
                    UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                }
            })
            alertController.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            self.present(alertController, animated: true)
        }
    }
}

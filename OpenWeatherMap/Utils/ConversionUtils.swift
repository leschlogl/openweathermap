//
//  ConversionUtils.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import Foundation

class ConversionUtils {

    public static func getDateFromUnixTimestamp(timestamp: Int?) -> Date? {
        if let timestamp = timestamp,
            let timeInterval = TimeInterval(exactly: timestamp) {
            return Date(timeIntervalSince1970: timeInterval)
        } else {
            return nil
        }
    }

    public static func dateFromString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm"
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter.string(from: date)
    }

    public static func timeFromString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter.string(from: date)
    }

    public static func dayOfTheWeekFromString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "E"
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter.string(from: date)
    }

    public static func normalizeTemperature(temperature: Float) -> String {
        return "\(String(format:"%.01f", temperature))°C"
    }

    public static func normalizeHumidity(humidity: Int) -> String {
        return "\(String(humidity))%"
    }

    public static func normalizePressure(pressure: Int) -> String {
        return "\(String(pressure)) hpa"
    }

    public static func getIconFromWeatherCondition(condition: String) -> String{
        switch condition {
        case "01d":
            return "☀️"
        case "01n":
            return "🌕"
        case "02d","02n":
            return "🌤"
        case "03d","03n":
            return "☁️"
        case "04d","04n":
            return "🌥"
        case "09d","09n":
            return "🌧"
        case "10d","10n":
            return "🌦"
        case "11d","11n":
            return "⛈"
        case "13d","13n":
            return "❄️"
        case "50d","50n":
            return "🌫"
        default:
            return "❓"
        }
    }
}

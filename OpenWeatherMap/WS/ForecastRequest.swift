//
//  ForecastRequest.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

class ForecastRequest: NSObject {

    static let sharedInstance = ForecastRequest()

    fileprivate let FORECAST_ENDPOINT: String = "forecast/daily"

    func getForecast(cityId: Int, completionHandler: @escaping (_ response: ForecastResponse?, _ error: Error?) -> Void){
        let url = "\(API_BASE_URL)/\(API_VERSION)/\(FORECAST_ENDPOINT)?id=\(cityId)&units=metric&lang=pt&cnt=8&appId=\(API_TOKEN)"

        Alamofire.request(url).responseObject { (response: DataResponse<ForecastResponse>) in
            if response.error == nil {
                let forecastResponse = response.result.value
                if var forecasts = forecastResponse?.forecasts {
                    //used to remove TODAY from forecasts
                    forecasts.removeFirst()
                    forecastResponse?.forecasts = forecasts
                }
                completionHandler(forecastResponse, nil)
            } else {
                completionHandler(nil, response.error)
            }
        }
    }
}

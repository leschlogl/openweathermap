//
//  WeatherRequest.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

class WeatherRequest: NSObject {

    static let sharedInstance = WeatherRequest()

    fileprivate let WEATHER_ENDPOINT: String = "group"

    func getWeathers(completionHandler: @escaping (_ response: WeatherResponse?, _ error: Error?) -> Void){
        let cities: String = CityEnum.allCases.map({"\($0.rawValue)"}).joined(separator: ",")
        let url = "\(API_BASE_URL)/\(API_VERSION)/\(WEATHER_ENDPOINT)?id=\(cities)&units=metric&lang=pt&appId=\(API_TOKEN)"

        Alamofire.request(url).responseObject { (response: DataResponse<WeatherResponse>) in
            if response.error == nil {
                let weatherResponse = response.result.value
                completionHandler(weatherResponse, nil)
            } else {
                completionHandler(nil, response.error)
            }
        }
    }
}

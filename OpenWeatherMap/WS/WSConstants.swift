//
//  WSConstants.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

import Foundation

let API_BASE_URL: String = "https://api.openweathermap.org/data"
let API_VERSION: String = "2.5"
let API_TOKEN: String = ""

//
//  CityEnum.swift
//  OpenWeatherMap
//
//  Created by Lucas Eduardo Schlögl on 21/12/2018.
//  Copyright © 2018 Lucas Eduardo Schlogl. All rights reserved.
//

enum CityEnum: String, CaseIterable {
    case CURITIBA = "6322752"
    case SAOPAULO = "3448439"
    case FLORIANOPOLIS = "3463237"
}
